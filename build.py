import os

root_path = os.path.dirname(os.path.abspath(__file__)) + '/'

packages = [x[0] for x in os.walk(root_path) if x[0] != '.']

packages = [x for x in packages if '.git' not in x and root_path != x]

print("root_path {0}".format(root_path))

print(packages)


def build(path: str):
    base_path = os.path.join(root_path, path)
    print("building package {0}".format(path))
    os.chdir(base_path)
    os.system('makepkg -cf --sign || echo "FAILED TO MAKE PACKAGE: {0}"'.format(path))
    print("done building package {0}".format(path))


def main():
    [build(package) for package in packages]



if __name__ == "__main__":
    main()